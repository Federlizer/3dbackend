use axum::{routing::get, Router};

// TODO: extract routers in own module

mod errors;
mod extractors;
mod handlers;

pub fn setup_router() -> Router {
    let api_router = Router::new().route("/", get(handlers::hello_world_handler));

    return api_router;
}
