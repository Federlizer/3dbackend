use std::error::Error;
use std::fmt;

#[derive(Debug, PartialEq)]
pub enum AuthenticationError {
    DecodingError(jsonwebtoken::errors::Error),

    ExpiredToken,

    InvalidAlgorithm(jsonwebtoken::Algorithm),
    InvalidClaims,
    InvalidKeyId,
    InvalidSignature,

    MissingKeyId,
}

impl fmt::Display for AuthenticationError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        // TODO: Write different error text based on the error variant
        match self {
            Self::DecodingError(err) => write!(f, "{}", err),

            Self::ExpiredToken => write!(f, "Token has expired"),

            Self::InvalidAlgorithm(alg) => write!(f, "Invalid signing algorithm used: {:?}", alg),
            Self::InvalidClaims => write!(f, "Invalid or malformed claims stored in the token"),
            Self::InvalidKeyId => {
                write!(f, "'kid' key doesn't match any of the required public keys")
            }
            Self::InvalidSignature => write!(f, "Signature wasn't able to be decoded"),

            Self::MissingKeyId => write!(f, "'kid' key is missing"),
        }
    }
}

impl Error for AuthenticationError {}
