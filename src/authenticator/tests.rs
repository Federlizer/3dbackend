use std::collections::HashMap;

use openssl::rsa::Rsa;
use serde::{Deserialize, Serialize};

use super::*;

struct AuthenticatorOpts {
    key_id: String,
    decoding_key: jsonwebtoken::DecodingKey,
    audience: String,
    issuer: String,
}

struct HeaderOpts {
    algorithm: jsonwebtoken::Algorithm,
    kid: Option<String>,
}

#[derive(Debug, Deserialize, Serialize)]
struct TestFirebaseClaims {
    exp: Option<i64>,
    iat: Option<i64>,
    aud: Option<String>,
    iss: Option<String>,
    sub: Option<String>,
    auth_time: Option<i64>,
}

fn setup_authenticator(opts: AuthenticatorOpts) -> FirebaseAuthenticator {
    let mut keys: HashMap<String, jsonwebtoken::DecodingKey> = HashMap::new();
    keys.insert(opts.key_id, opts.decoding_key);

    let authenticator = FirebaseAuthenticator::new_with_keys(keys, &opts.audience, &opts.issuer);

    return authenticator;
}

fn setup_header(opts: HeaderOpts) -> jsonwebtoken::Header {
    let mut header = jsonwebtoken::Header::new(opts.algorithm);
    header.kid = opts.kid;

    return header;
}

fn setup_rsa_keys() -> (jsonwebtoken::EncodingKey, jsonwebtoken::DecodingKey) {
    let key = Rsa::generate(2048).unwrap();

    let private_key_pem = key.private_key_to_pem().unwrap();
    let public_key_pem = key.public_key_to_pem().unwrap();

    let decoding_key = jsonwebtoken::DecodingKey::from_rsa_pem(public_key_pem.as_slice()).unwrap();
    let encoding_key = jsonwebtoken::EncodingKey::from_rsa_pem(private_key_pem.as_slice()).unwrap();

    return (encoding_key, decoding_key);
}

#[test]
fn authenticates_valid_token() {
    let now_timestamp = chrono::Utc::now().timestamp();
    let key_id = String::from("1234567890");
    let audience = String::from("fir-poc-de65e");
    let issuer = String::from("https://securetoken.google.com/fir-poc-de65e");
    let subject = String::from("asdlfoiausofiuvhosairuhtgsarjgnlsifdjhb");

    let (encoding_key, decoding_key) = setup_rsa_keys();

    let authenticator = setup_authenticator(AuthenticatorOpts {
        key_id: key_id.clone(),
        decoding_key: decoding_key.clone(),
        audience: audience.clone(),
        issuer: issuer.clone(),
    });

    let header = setup_header(HeaderOpts {
        algorithm: jsonwebtoken::Algorithm::RS256,
        kid: Some(key_id.clone()),
    });

    let test_claims = TestFirebaseClaims {
        exp: Some(now_timestamp + 3600),
        iat: Some(now_timestamp - 1), // Tests can be too quick, give it at least a second
        auth_time: Some(now_timestamp - 1), // Test can be too quick, give it at least a second
        aud: Some(audience.clone()),
        iss: Some(issuer.clone()),
        sub: Some(subject.clone()),
    };

    let jwt = jsonwebtoken::encode(&header, &test_claims, &encoding_key).unwrap();

    let validated_claims = match authenticator.validate(jwt.as_str()) {
        Ok(claims) => claims,
        Err(err) => {
            panic!("{err}");
        }
    };

    assert_eq!(test_claims.exp, Some(validated_claims.exp));
    assert_eq!(test_claims.iat, Some(validated_claims.iat));
    assert_eq!(test_claims.aud, Some(validated_claims.aud));
    assert_eq!(test_claims.iss, Some(validated_claims.iss));
    assert_eq!(test_claims.sub, Some(validated_claims.sub));
    assert_eq!(test_claims.auth_time, Some(validated_claims.auth_time));
}

#[test]
fn denies_malformed_token() {
    let key_id = String::from("1234567890");
    let audience = String::from("fir-poc-de65e");
    let issuer = String::from("https://securetoken.google.com/fir-poc-de65e");

    let (_, decoding_key) = setup_rsa_keys();

    let authenticator = setup_authenticator(AuthenticatorOpts {
        key_id: key_id.clone(),
        decoding_key: decoding_key.clone(),
        audience: audience.clone(),
        issuer: issuer.clone(),
    });

    let malformed_jwt = String::from("eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzUxMiIsImtpZCI6IjEyMzQ1Njc4OTAifQeyJleHAiOjE2ODc1NDcwMjYsImlhdCI6MTY4NzU0MzQyNSwiYXVkIjoiZmlyLXBvYy1kZTY1ZSIsImlzcyI6Imh0dHBzOi8vc2VjdXJldG9rZW4uZ29vZ2xlLmNvbS9maXItcG9jLWRlNjVlIiwic3ViIjoiYXNkbGZvaWF1c29maXV2aG9zYWlydWh0Z3NhcmpnbmxzaWZkamhiIiwiYXV0aF90aW1lIjoxNjg3NTQzNDI1fQ.lEU9UiRAxohhFdlU_qF2C5Y8HGOVFIpoPRuzVcQAXhmRBYTBZ1T7Au2sbpnDyB9NlNieBzmaIMSvLvyNa_l5BCYRR8LRxjZbcMWUdqa3rp9wLMN5f5JBIjh7Jd-zPFLxW6stTi-DE77uI6UFfaer2qB-lbHkcmECxXRY-GupL4kY7BS0ZIwXeqc_1SBhVCykJIuV-tpPzL2HcndE4XH1c8SCRl7tWlEShPKx68NKpWgXCpogBCGJ06X85wpCyx7io1ogcTQwnCxE2HIm8Blo6uJGHyCjaBw5NuCLFnsgStrKsemY_elb-1hzPBkh5AEpmYx-TxkHszeJQxaEOSHs_A");

    let res = authenticator.validate(malformed_jwt.as_str());

    match res {
        Ok(claims) => panic!(
            "Malformed token was accepted by validator and returned claims {:?}",
            claims
        ),
        Err(err) => match err {
            AuthenticationError::DecodingError(_) => return,
            _ => panic!("Didn't receive a decoding error as expected"),
        },
    }
}

#[test]
fn denies_token_with_incorrect_algorithm() {
    let now_timestamp = chrono::Utc::now().timestamp();
    let key_id = String::from("1234567890");
    let audience = String::from("fir-poc-de65e");
    let issuer = String::from("https://securetoken.google.com/fir-poc-de65e");
    let subject = String::from("asdlfoiausofiuvhosairuhtgsarjgnlsifdjhb");

    let (encoding_key, decoding_key) = setup_rsa_keys();

    let authenticator = setup_authenticator(AuthenticatorOpts {
        key_id: key_id.clone(),
        decoding_key: decoding_key.clone(),
        audience: audience.clone(),
        issuer: issuer.clone(),
    });

    let header = setup_header(HeaderOpts {
        algorithm: jsonwebtoken::Algorithm::RS512,
        kid: Some(key_id.clone()),
    });

    let claims = TestFirebaseClaims {
        exp: Some(now_timestamp + 3600),
        iat: Some(now_timestamp - 1), // Tests can be too quick, give it at least a second
        auth_time: Some(now_timestamp - 1), // Test can be too quick, give it at least a second
        aud: Some(audience.clone()),
        iss: Some(issuer.clone()),
        sub: Some(subject.clone()),
    };

    let jwt = jsonwebtoken::encode(&header, &claims, &encoding_key).unwrap();

    let res = authenticator.validate(jwt.as_str());

    assert_eq!(res, Err(AuthenticationError::InvalidAlgorithm(header.alg)));
}

#[test]
fn denies_token_with_missing_key_id() {
    let now_timestamp = chrono::Utc::now().timestamp();
    let key_id = String::from("1234567890");
    let audience = String::from("fir-poc-de65e");
    let issuer = String::from("https://securetoken.google.com/fir-poc-de65e");
    let subject = String::from("asdlfoiausofiuvhosairuhtgsarjgnlsifdjhb");

    let (encoding_key, decoding_key) = setup_rsa_keys();

    let authenticator = setup_authenticator(AuthenticatorOpts {
        key_id: key_id.clone(),
        decoding_key: decoding_key.clone(),
        audience: audience.clone(),
        issuer: issuer.clone(),
    });

    let header = setup_header(HeaderOpts {
        algorithm: jsonwebtoken::Algorithm::RS256,
        kid: None,
    });

    let claims = TestFirebaseClaims {
        exp: Some(now_timestamp + 3600),
        iat: Some(now_timestamp - 1), // Tests can be too quick, give it at least a second
        auth_time: Some(now_timestamp - 1), // Test can be too quick, give it at least a second
        aud: Some(audience.clone()),
        iss: Some(issuer.clone()),
        sub: Some(subject.clone()),
    };

    let jwt = jsonwebtoken::encode(&header, &claims, &encoding_key).unwrap();

    let res = authenticator.validate(jwt.as_str());

    assert_eq!(res, Err(AuthenticationError::MissingKeyId));
}
#[test]
fn denies_token_with_incorrect_key_id() {
    let now_timestamp = chrono::Utc::now().timestamp();
    let key_id = String::from("1234567890");
    let incorrect_key_id = String::from("0987654321");
    let audience = String::from("fir-poc-de65e");
    let issuer = String::from("https://securetoken.google.com/fir-poc-de65e");
    let subject = String::from("asdlfoiausofiuvhosairuhtgsarjgnlsifdjhb");

    let (encoding_key, decoding_key) = setup_rsa_keys();

    let authenticator = setup_authenticator(AuthenticatorOpts {
        key_id: key_id.clone(),
        decoding_key: decoding_key.clone(),
        audience: audience.clone(),
        issuer: issuer.clone(),
    });

    let header = setup_header(HeaderOpts {
        algorithm: jsonwebtoken::Algorithm::RS256,
        kid: Some(incorrect_key_id),
    });

    let claims = TestFirebaseClaims {
        exp: Some(now_timestamp + 3600),
        iat: Some(now_timestamp - 1), // Tests can be too quick, give it at least a second
        auth_time: Some(now_timestamp - 1), // Test can be too quick, give it at least a second
        aud: Some(audience.clone()),
        iss: Some(issuer.clone()),
        sub: Some(subject.clone()),
    };

    let jwt = jsonwebtoken::encode(&header, &claims, &encoding_key).unwrap();

    let res = authenticator.validate(jwt.as_str());

    assert_eq!(res, Err(AuthenticationError::InvalidKeyId));
}

#[test]
fn denies_token_with_invalid_signature() {
    let now_timestamp = chrono::Utc::now().timestamp();
    let key_id = String::from("1234567890");
    let audience = String::from("fir-poc-de65e");
    let issuer = String::from("https://securetoken.google.com/fir-poc-de65e");
    let subject = String::from("asdlfoiausofiuvhosairuhtgsarjgnlsifdjhb");

    let (encoding_key, _) = setup_rsa_keys();
    let (_, alt_decoding_key) = setup_rsa_keys();

    let authenticator = setup_authenticator(AuthenticatorOpts {
        key_id: key_id.clone(),
        decoding_key: alt_decoding_key.clone(),
        audience: audience.clone(),
        issuer: issuer.clone(),
    });

    let header = setup_header(HeaderOpts {
        algorithm: jsonwebtoken::Algorithm::RS256,
        kid: Some(key_id),
    });

    let claims = TestFirebaseClaims {
        exp: Some(now_timestamp + 3600),
        iat: Some(now_timestamp - 1), // Tests can be too quick, give it at least a second
        auth_time: Some(now_timestamp - 1), // Test can be too quick, give it at least a second
        aud: Some(audience.clone()),
        iss: Some(issuer.clone()),
        sub: Some(subject.clone()),
    };

    let jwt = jsonwebtoken::encode(&header, &claims, &encoding_key).unwrap();

    let res = authenticator.validate(jwt.as_str());

    assert_eq!(res, Err(AuthenticationError::InvalidSignature));
}
#[test]
fn denies_token_with_missing_expiration_claim() {
    let now_timestamp = chrono::Utc::now().timestamp();
    let key_id = String::from("1234567890");
    let audience = String::from("fir-poc-de65e");
    let issuer = String::from("https://securetoken.google.com/fir-poc-de65e");
    let subject = String::from("asdlfoiausofiuvhosairuhtgsarjgnlsifdjhb");

    let (encoding_key, decoding_key) = setup_rsa_keys();

    let authenticator = setup_authenticator(AuthenticatorOpts {
        key_id: key_id.clone(),
        decoding_key: decoding_key.clone(),
        audience: audience.clone(),
        issuer: issuer.clone(),
    });

    let header = setup_header(HeaderOpts {
        algorithm: jsonwebtoken::Algorithm::RS256,
        kid: Some(key_id.clone()),
    });

    let claims = TestFirebaseClaims {
        exp: None,
        iat: Some(now_timestamp - 1), // Tests can be too quick, give it at least a second
        auth_time: Some(now_timestamp - 1), // Test can be too quick, give it at least a second
        aud: Some(audience.clone()),
        iss: Some(issuer.clone()),
        sub: Some(subject.clone()),
    };

    let jwt = jsonwebtoken::encode(&header, &claims, &encoding_key).unwrap();

    let res = authenticator.validate(jwt.as_str());

    assert_eq!(res, Err(AuthenticationError::InvalidClaims));
}
#[test]
fn denies_token_with_missing_issued_at_claim() {
    let now_timestamp = chrono::Utc::now().timestamp();
    let key_id = String::from("1234567890");
    let audience = String::from("fir-poc-de65e");
    let issuer = String::from("https://securetoken.google.com/fir-poc-de65e");
    let subject = String::from("asdlfoiausofiuvhosairuhtgsarjgnlsifdjhb");

    let (encoding_key, decoding_key) = setup_rsa_keys();

    let authenticator = setup_authenticator(AuthenticatorOpts {
        key_id: key_id.clone(),
        decoding_key: decoding_key.clone(),
        audience: audience.clone(),
        issuer: issuer.clone(),
    });

    let header = setup_header(HeaderOpts {
        algorithm: jsonwebtoken::Algorithm::RS256,
        kid: Some(key_id.clone()),
    });

    let claims = TestFirebaseClaims {
        exp: Some(now_timestamp + 3600),
        iat: None,
        auth_time: Some(now_timestamp - 1), // Test can be too quick, give it at least a second
        aud: Some(audience.clone()),
        iss: Some(issuer.clone()),
        sub: Some(subject.clone()),
    };

    let jwt = jsonwebtoken::encode(&header, &claims, &encoding_key).unwrap();

    let res = authenticator.validate(jwt.as_str());

    assert_eq!(res, Err(AuthenticationError::InvalidClaims));
}
#[test]
fn denies_token_with_missing_audience_claim() {
    let now_timestamp = chrono::Utc::now().timestamp();
    let key_id = String::from("1234567890");
    let audience = String::from("fir-poc-de65e");
    let issuer = String::from("https://securetoken.google.com/fir-poc-de65e");
    let subject = String::from("asdlfoiausofiuvhosairuhtgsarjgnlsifdjhb");

    let (encoding_key, decoding_key) = setup_rsa_keys();

    let authenticator = setup_authenticator(AuthenticatorOpts {
        key_id: key_id.clone(),
        decoding_key: decoding_key.clone(),
        audience: audience.clone(),
        issuer: issuer.clone(),
    });

    let header = setup_header(HeaderOpts {
        algorithm: jsonwebtoken::Algorithm::RS256,
        kid: Some(key_id.clone()),
    });

    let claims = TestFirebaseClaims {
        exp: Some(now_timestamp + 3600),
        iat: Some(now_timestamp - 1), // Tests can be too quick, give it at least a second
        auth_time: Some(now_timestamp - 1), // Test can be too quick, give it at least a second
        aud: None,
        iss: Some(issuer.clone()),
        sub: Some(subject.clone()),
    };

    let jwt = jsonwebtoken::encode(&header, &claims, &encoding_key).unwrap();

    let res = authenticator.validate(jwt.as_str());

    assert_eq!(res, Err(AuthenticationError::InvalidClaims));
}

#[test]
fn denies_token_with_missing_issuer_claim() {
    let now_timestamp = chrono::Utc::now().timestamp();
    let key_id = String::from("1234567890");
    let audience = String::from("fir-poc-de65e");
    let issuer = String::from("https://securetoken.google.com/fir-poc-de65e");
    let subject = String::from("asdlfoiausofiuvhosairuhtgsarjgnlsifdjhb");

    let (encoding_key, decoding_key) = setup_rsa_keys();

    let authenticator = setup_authenticator(AuthenticatorOpts {
        key_id: key_id.clone(),
        decoding_key: decoding_key.clone(),
        audience: audience.clone(),
        issuer: issuer.clone(),
    });

    let header = setup_header(HeaderOpts {
        algorithm: jsonwebtoken::Algorithm::RS256,
        kid: Some(key_id.clone()),
    });

    let claims = TestFirebaseClaims {
        exp: Some(now_timestamp + 3600),
        iat: Some(now_timestamp - 1), // Tests can be too quick, give it at least a second
        auth_time: Some(now_timestamp - 1), // Test can be too quick, give it at least a second
        aud: Some(audience.clone()),
        iss: None,
        sub: Some(subject.clone()),
    };

    let jwt = jsonwebtoken::encode(&header, &claims, &encoding_key).unwrap();

    let res = authenticator.validate(jwt.as_str());

    assert_eq!(res, Err(AuthenticationError::InvalidClaims));
}

#[test]
fn denies_token_with_missing_subject_claim() {
    let now_timestamp = chrono::Utc::now().timestamp();
    let key_id = String::from("1234567890");
    let audience = String::from("fir-poc-de65e");
    let issuer = String::from("https://securetoken.google.com/fir-poc-de65e");
    let _subject = String::from("asdlfoiausofiuvhosairuhtgsarjgnlsifdjhb");

    let (encoding_key, decoding_key) = setup_rsa_keys();

    let authenticator = setup_authenticator(AuthenticatorOpts {
        key_id: key_id.clone(),
        decoding_key: decoding_key.clone(),
        audience: audience.clone(),
        issuer: issuer.clone(),
    });

    let header = setup_header(HeaderOpts {
        algorithm: jsonwebtoken::Algorithm::RS256,
        kid: Some(key_id.clone()),
    });

    let claims = TestFirebaseClaims {
        exp: Some(now_timestamp + 3600),
        iat: Some(now_timestamp - 1), // Tests can be too quick, give it at least a second
        auth_time: Some(now_timestamp - 1), // Test can be too quick, give it at least a second
        aud: Some(audience.clone()),
        iss: Some(issuer.clone()),
        sub: None,
    };

    let jwt = jsonwebtoken::encode(&header, &claims, &encoding_key).unwrap();

    let res = authenticator.validate(jwt.as_str());

    assert_eq!(res, Err(AuthenticationError::InvalidClaims));
}
#[test]
fn denies_token_with_missing_auth_time_claim() {
    let now_timestamp = chrono::Utc::now().timestamp();
    let key_id = String::from("1234567890");
    let audience = String::from("fir-poc-de65e");
    let issuer = String::from("https://securetoken.google.com/fir-poc-de65e");
    let subject = String::from("asdlfoiausofiuvhosairuhtgsarjgnlsifdjhb");

    let (encoding_key, decoding_key) = setup_rsa_keys();

    let authenticator = setup_authenticator(AuthenticatorOpts {
        key_id: key_id.clone(),
        decoding_key: decoding_key.clone(),
        audience: audience.clone(),
        issuer: issuer.clone(),
    });

    let header = setup_header(HeaderOpts {
        algorithm: jsonwebtoken::Algorithm::RS256,
        kid: Some(key_id.clone()),
    });

    let claims = TestFirebaseClaims {
        exp: Some(now_timestamp + 3600),
        iat: Some(now_timestamp - 1), // Tests can be too quick, give it at least a second
        auth_time: None,
        aud: Some(audience.clone()),
        iss: Some(issuer.clone()),
        sub: Some(subject.clone()),
    };

    let jwt = jsonwebtoken::encode(&header, &claims, &encoding_key).unwrap();

    let res = authenticator.validate(jwt.as_str());

    assert_eq!(res, Err(AuthenticationError::InvalidClaims));
}

#[test]
fn denies_token_with_expiration_in_the_past() {
    let now_timestamp = chrono::Utc::now().timestamp();
    let key_id = String::from("1234567890");
    let audience = String::from("fir-poc-de65e");
    let issuer = String::from("https://securetoken.google.com/fir-poc-de65e");
    let subject = String::from("asdlfoiausofiuvhosairuhtgsarjgnlsifdjhb");

    let (encoding_key, decoding_key) = setup_rsa_keys();

    let authenticator = setup_authenticator(AuthenticatorOpts {
        key_id: key_id.clone(),
        decoding_key: decoding_key.clone(),
        audience: audience.clone(),
        issuer: issuer.clone(),
    });

    let header = setup_header(HeaderOpts {
        algorithm: jsonwebtoken::Algorithm::RS256,
        kid: Some(key_id.clone()),
    });

    let claims = TestFirebaseClaims {
        exp: Some(now_timestamp - 1800),
        iat: Some(now_timestamp - 1),
        auth_time: Some(now_timestamp - 1),
        aud: Some(audience.clone()),
        iss: Some(issuer.clone()),
        sub: Some(subject.clone()),
    };

    let jwt = jsonwebtoken::encode(&header, &claims, &encoding_key).unwrap();

    let res = authenticator.validate(jwt.as_str());

    assert_eq!(res, Err(AuthenticationError::ExpiredToken));
}

#[test]
fn denies_token_with_issued_at_in_the_future() {
    let now_timestamp = chrono::Utc::now().timestamp();
    let key_id = String::from("1234567890");
    let audience = String::from("fir-poc-de65e");
    let issuer = String::from("https://securetoken.google.com/fir-poc-de65e");
    let subject = String::from("asdlfoiausofiuvhosairuhtgsarjgnlsifdjhb");

    let (encoding_key, decoding_key) = setup_rsa_keys();

    let authenticator = setup_authenticator(AuthenticatorOpts {
        key_id: key_id.clone(),
        decoding_key: decoding_key.clone(),
        audience: audience.clone(),
        issuer: issuer.clone(),
    });

    let header = setup_header(HeaderOpts {
        algorithm: jsonwebtoken::Algorithm::RS256,
        kid: Some(key_id.clone()),
    });

    let claims = TestFirebaseClaims {
        exp: Some(now_timestamp + 3600),
        iat: Some(now_timestamp + 1800), // Tests can be too quick, give it at least a second
        auth_time: Some(now_timestamp - 1), // Test can be too quick, give it at least a second
        aud: Some(audience.clone()),
        iss: Some(issuer.clone()),
        sub: Some(subject.clone()),
    };

    let jwt = jsonwebtoken::encode(&header, &claims, &encoding_key).unwrap();

    let res = authenticator.validate(jwt.as_str());

    assert_eq!(res, Err(AuthenticationError::InvalidClaims));
}

#[test]
fn denies_token_with_auth_time_in_the_future() {
    let now_timestamp = chrono::Utc::now().timestamp();
    let key_id = String::from("1234567890");
    let audience = String::from("fir-poc-de65e");
    let issuer = String::from("https://securetoken.google.com/fir-poc-de65e");
    let subject = String::from("asdlfoiausofiuvhosairuhtgsarjgnlsifdjhb");

    let (encoding_key, decoding_key) = setup_rsa_keys();

    let authenticator = setup_authenticator(AuthenticatorOpts {
        key_id: key_id.clone(),
        decoding_key: decoding_key.clone(),
        audience: audience.clone(),
        issuer: issuer.clone(),
    });

    let header = setup_header(HeaderOpts {
        algorithm: jsonwebtoken::Algorithm::RS256,
        kid: Some(key_id.clone()),
    });

    let claims = TestFirebaseClaims {
        exp: Some(now_timestamp + 3600),
        iat: Some(now_timestamp - 1), // Tests can be too quick, give it at least a second
        auth_time: Some(now_timestamp + 1800), // Test can be too quick, give it at least a second
        aud: Some(audience.clone()),
        iss: Some(issuer.clone()),
        sub: Some(subject.clone()),
    };

    let jwt = jsonwebtoken::encode(&header, &claims, &encoding_key).unwrap();

    let res = authenticator.validate(jwt.as_str());

    assert_eq!(res, Err(AuthenticationError::InvalidClaims));
}

#[test]
fn denies_token_with_incorrect_audience() {
    let now_timestamp = chrono::Utc::now().timestamp();
    let key_id = String::from("1234567890");
    let audience = String::from("fir-poc-de65e");
    let incorrect_audience = String::from("fir-poc-de65ee");
    let issuer = String::from("https://securetoken.google.com/fir-poc-de65e");
    let subject = String::from("asdlfoiausofiuvhosairuhtgsarjgnlsifdjhb");

    let (encoding_key, decoding_key) = setup_rsa_keys();

    let authenticator = setup_authenticator(AuthenticatorOpts {
        key_id: key_id.clone(),
        decoding_key: decoding_key.clone(),
        audience: audience.clone(),
        issuer: issuer.clone(),
    });

    let header = setup_header(HeaderOpts {
        algorithm: jsonwebtoken::Algorithm::RS256,
        kid: Some(key_id.clone()),
    });

    let claims = TestFirebaseClaims {
        exp: Some(now_timestamp + 3600),
        iat: Some(now_timestamp - 1), // Tests can be too quick, give it at least a second
        auth_time: Some(now_timestamp - 1), // Test can be too quick, give it at least a second
        aud: Some(incorrect_audience.clone()),
        iss: Some(issuer.clone()),
        sub: Some(subject.clone()),
    };

    let jwt = jsonwebtoken::encode(&header, &claims, &encoding_key).unwrap();

    let res = authenticator.validate(jwt.as_str());

    assert_eq!(res, Err(AuthenticationError::InvalidClaims));
}

#[test]
fn denies_token_with_incorrect_issuer() {
    let now_timestamp = chrono::Utc::now().timestamp();
    let key_id = String::from("1234567890");
    let audience = String::from("fir-poc-de65e");
    let issuer = String::from("https://securetoken.google.com/fir-poc-de65e");
    let incorrect_issuer = String::from("https://securetoken.google.com/fir-poc-de65e3");
    let subject = String::from("asdlfoiausofiuvhosairuhtgsarjgnlsifdjhb");

    let (encoding_key, decoding_key) = setup_rsa_keys();

    let authenticator = setup_authenticator(AuthenticatorOpts {
        key_id: key_id.clone(),
        decoding_key: decoding_key.clone(),
        audience: audience.clone(),
        issuer: issuer.clone(),
    });

    let header = setup_header(HeaderOpts {
        algorithm: jsonwebtoken::Algorithm::RS256,
        kid: Some(key_id.clone()),
    });

    let claims = TestFirebaseClaims {
        exp: Some(now_timestamp + 3600),
        iat: Some(now_timestamp - 1), // Tests can be too quick, give it at least a second
        auth_time: Some(now_timestamp - 1), // Test can be too quick, give it at least a second
        aud: Some(audience.clone()),
        iss: Some(incorrect_issuer.clone()),
        sub: Some(subject.clone()),
    };

    let jwt = jsonwebtoken::encode(&header, &claims, &encoding_key).unwrap();

    let res = authenticator.validate(jwt.as_str());

    assert_eq!(res, Err(AuthenticationError::InvalidClaims));
}

#[test]
fn denies_token_with_empty_subject() {
    let now_timestamp = chrono::Utc::now().timestamp();
    let key_id = String::from("1234567890");
    let audience = String::from("fir-poc-de65e");
    let issuer = String::from("https://securetoken.google.com/fir-poc-de65e");
    let subject = String::from("");

    let (encoding_key, decoding_key) = setup_rsa_keys();

    let authenticator = setup_authenticator(AuthenticatorOpts {
        key_id: key_id.clone(),
        decoding_key: decoding_key.clone(),
        audience: audience.clone(),
        issuer: issuer.clone(),
    });

    let header = setup_header(HeaderOpts {
        algorithm: jsonwebtoken::Algorithm::RS256,
        kid: Some(key_id.clone()),
    });

    let claims = TestFirebaseClaims {
        exp: Some(now_timestamp + 3600),
        iat: Some(now_timestamp - 1), // Tests can be too quick, give it at least a second
        auth_time: Some(now_timestamp - 1), // Test can be too quick, give it at least a second
        aud: Some(audience.clone()),
        iss: Some(issuer.clone()),
        sub: Some(subject.clone()),
    };

    let jwt = jsonwebtoken::encode(&header, &claims, &encoding_key).unwrap();

    let res = authenticator.validate(jwt.as_str());

    assert_eq!(res, Err(AuthenticationError::InvalidClaims));
}
