use axum::http::StatusCode;
use axum::response::{IntoResponse, Response};

pub enum ApiError {
    Unauthorized,
    BadRequest(String),
}

impl IntoResponse for ApiError {
    fn into_response(self) -> Response {
        let (status, message) = match self {
            Self::Unauthorized => (
                StatusCode::UNAUTHORIZED,
                String::from("Missing or invalid credentials"),
            ),
            Self::BadRequest(error_code) => (
                StatusCode::BAD_REQUEST,
                format!("{{ \"error_code\": {error_code} }}"),
            ),
        };

        return (status, message).into_response();
    }
}
