use crate::api::extractors::UserCredentials;

pub async fn hello_world_handler(user_credentials: UserCredentials) -> String {
    format!(
        "Hello {} with user ID {}",
        user_credentials.email, user_credentials.uid
    )
}
