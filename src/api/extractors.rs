use axum::extract::{rejection::TypedHeaderRejectionReason, TypedHeader};
use axum::headers::{authorization::Bearer, Authorization};
use axum::http::request::Parts;
use axum::RequestPartsExt;
use axum::{async_trait, extract::FromRequestParts};

use crate::api::errors::ApiError;
use crate::authenticator::{FirebaseAuthenticator, FIREBASE_AUDIENCE_CLAIM, FIREBASE_ISSUER_CLAIM};

pub struct UserCredentials {
    pub uid: String,
    pub email: String,
    pub token: String,
}

impl UserCredentials {
    pub fn new(uid: &str, email: &str, token: &str) -> UserCredentials {
        UserCredentials {
            uid: String::from(uid),
            email: String::from(email),
            token: String::from(token),
        }
    }
}

#[async_trait]
impl<S> FromRequestParts<S> for UserCredentials
where
    S: Send + Sync,
{
    type Rejection = ApiError;

    async fn from_request_parts(parts: &mut Parts, _state: &S) -> Result<Self, Self::Rejection> {
        // Extract the Authorization header
        let authorization_header = parts
            .extract::<TypedHeader<Authorization<Bearer>>>()
            .await
            .map_err(|err| match err.reason() {
                TypedHeaderRejectionReason::Missing => ApiError::Unauthorized,
                TypedHeaderRejectionReason::Error(err) => {
                    println!("Failed to parse Authorization header: {err}");
                    ApiError::Unauthorized
                }
                _ => {
                    println!("Unknown error occurred while parsing Authorization header: {err}");
                    ApiError::Unauthorized
                }
            })?;

        let token = authorization_header.token();
        println!("Received token {token}");

        // ensure fb auth is setup
        let fb_authenticator =
            FirebaseAuthenticator::new(FIREBASE_AUDIENCE_CLAIM, FIREBASE_ISSUER_CLAIM).await;

        // validate the bearer token
        let claims = fb_authenticator
            .validate(token)
            .map_err(|_| ApiError::Unauthorized)?;

        let user_credentials =
            UserCredentials::new(claims.sub.as_str(), "not@implemented.com", token);

        // return user credentials
        Ok(user_credentials)
    }
}
