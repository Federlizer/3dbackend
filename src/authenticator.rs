///! Authenticate on backend:
///! https://firebase.google.com/docs/auth/admin/verify-id-tokens#verify_id_tokens_using_a_third-party_jwt_library
///! We must be sending the JWT (idToken) to the backend. That looks to be enough.
use chrono::Utc;
use jsonwebtoken::{decode, decode_header, Algorithm, Validation};
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::collections::{HashMap, HashSet};
use std::error::Error;

mod errors;

use errors::AuthenticationError;

pub const FIREBASE_PUBLIC_KEYS_URL: &str =
    "https://www.googleapis.com/robot/v1/metadata/x509/securetoken@system.gserviceaccount.com";
pub const FIREBASE_AUDIENCE_CLAIM: &str = "printabbble-poc";
pub const FIREBASE_ISSUER_CLAIM: &str = "https://securetoken.google.com/printabbble-poc";

#[derive(PartialEq, Debug, Serialize, Deserialize)]
pub struct FirebaseClaims {
    pub exp: i64,
    pub iat: i64,
    pub aud: String,
    pub iss: String,
    pub sub: String,
    pub auth_time: i64,
}

pub struct FirebaseAuthenticator {
    keys: HashMap<String, jsonwebtoken::DecodingKey>,

    audience: String,
    issuer: String,
}

impl FirebaseAuthenticator {
    pub async fn new(aud: &str, iss: &str) -> Self {
        // Fetch keys first
        let keys = match FirebaseAuthenticator::fetch_validation_keys().await {
            Ok(keys) => keys,
            Err(err) => panic!("Error fetching validation keys: {err}"),
        };

        let fb_authenticator = FirebaseAuthenticator::new_with_keys(keys, aud, iss);

        return fb_authenticator;
    }

    pub fn new_with_keys(
        keys: HashMap<String, jsonwebtoken::DecodingKey>,
        aud: &str,
        iss: &str,
    ) -> Self {
        FirebaseAuthenticator {
            keys,
            audience: String::from(aud),
            issuer: String::from(iss),
        }
    }

    async fn fetch_validation_keys(
    ) -> Result<HashMap<String, jsonwebtoken::DecodingKey>, Box<dyn Error>> {
        let response = reqwest::get(FIREBASE_PUBLIC_KEYS_URL).await?;

        // TODO: Read and store the cache-control max-age value
        let headers = response.headers();

        let cache_control = match headers.get("Cache-Control") {
            Some(header) => header,
            None => panic!("Missing cache-control from Firebase Public Key response"),
        };

        println!("{:?}", cache_control);

        let body_text = response.text().await?;
        let json_body: Value = match serde_json::from_str(body_text.as_str()) {
            Ok(val) => val,
            Err(err) => {
                panic!("{err}");
            }
        };

        let obj = match json_body.as_object() {
            Some(obj) => obj,
            None => {
                panic!("Didn't receive an object");
            }
        };

        let mut keys: HashMap<String, jsonwebtoken::DecodingKey> = HashMap::new();
        for (key, val) in obj.iter() {
            let key_id = String::from(key);

            let decoding_key = match val.as_str() {
                Some(val) => jsonwebtoken::DecodingKey::from_rsa_pem(val.as_bytes())?,
                None => panic!("{} is not a valid string", val),
            };

            keys.insert(key_id, decoding_key);
        }

        Ok(keys)
    }

    /// Validates the header of a Firebase JWT. Expects the header to include the following keys:
    ///
    /// - `alg` (Algorithm) - containing "RS256" as its value
    /// - `kid` (Key ID) - field containing the key used to sign the JWT. The key must be one of the public Google
    ///    Firebase keys
    fn validate_header(&self, jwt: &str) -> Result<jsonwebtoken::Header, AuthenticationError> {
        // Decode header
        let header = match decode_header(jwt) {
            Ok(header) => header,
            Err(err) => {
                println!("Unable to decode header due to jsonwebtoken error: {err}");
                return Err(AuthenticationError::DecodingError(err));
            }
        };

        // Algorithm must be RS256
        if header.alg != jsonwebtoken::Algorithm::RS256 {
            println!("Invalid algorithm passed in JWT: {:?}", header.alg);
            return Err(AuthenticationError::InvalidAlgorithm(header.alg));
        }

        // kid must exist
        let kid = match &header.kid {
            Some(kid) => kid,
            None => {
                println!("'kid' key doesn't exist, invalid token");
                return Err(AuthenticationError::MissingKeyId);
            }
        };

        // kid must also be one of the available keys from Google Firebase
        let mut key_id_found = false;
        for (key_id, _) in self.keys.iter() {
            if kid == key_id {
                key_id_found = true;
                break;
            }
        }

        // Only validate header if the kid exists in our own cache
        if key_id_found {
            return Ok(header);
        } else {
            println!("Key ID {kid} not found in stored keys");
            return Err(AuthenticationError::InvalidKeyId);
        }
    }

    /// Validates the signature of a Firebase JWT. Doesn't perform any of the jsonwebtoken checks,
    /// instead it just verifies that the signature of the `jwt` matches the `key` passed.
    /// If the `jwt` is able to be decoded using the `key`, a full
    /// jsonwebtoken::TokenData<FirebaseClaims> object is returned.
    fn validate_signature(
        &self,
        jwt: &str,
        key: &jsonwebtoken::DecodingKey,
    ) -> Result<jsonwebtoken::TokenData<FirebaseClaims>, AuthenticationError> {
        // Only RS256 is supported for signatures here
        let mut validation = Validation::new(Algorithm::RS256);

        // Don't require or validate any claims, because we are only validating the signature here.
        // Claim validation is later
        validation.required_spec_claims = HashSet::new();
        validation.validate_exp = false;
        validation.validate_nbf = false;

        let token = match decode::<FirebaseClaims>(jwt, key, &validation) {
            Ok(token) => token,
            Err(err) => match err.kind() {
                jsonwebtoken::errors::ErrorKind::InvalidSignature => {
                    println!("Invalid JWT signature");
                    return Err(AuthenticationError::InvalidSignature);
                }
                jsonwebtoken::errors::ErrorKind::Json(err) => {
                    println!("Missing required claim: {err}");
                    return Err(AuthenticationError::InvalidClaims);
                }
                _ => {
                    println!("Failed to decode JWT: {err}");
                    return Err(AuthenticationError::DecodingError(err));
                }
            },
        };

        return Ok(token);
    }

    /// Validates the claims passed are valid based on below criteria.
    ///
    /// Verify the ID token's payload conforms to the following constraints:
    /// exp       Expiration time         Must be in the future. The time is measured in seconds since the UNIX epoch.
    /// iat       Issued-at time          Must be in the past. The time is measured in seconds since the UNIX epoch.
    /// aud       Audience                Must be your Firebase project ID, the unique identifier for your Firebase project, which can be found in the URL of that project's console.
    /// iss       Issuer                  Must be "https://securetoken.google.com/<projectId>", where <projectId> is the same project ID used for aud above.
    /// sub       Subject                 Must be a non-empty string and must be the uid of the user or device.
    /// auth_time Authentication time     Must be in the past. The time when the user authenticated.
    ///
    /// If all the above verifications are successful, you can use the subject (sub) of the ID token as the uid of the corresponding user or device.
    fn validate_claims(&self, claims: &FirebaseClaims) -> Result<(), AuthenticationError> {
        let now = Utc::now().timestamp();

        // Must be in the future. The time is measured in seconds since the UNIX epoch.
        if !(now < claims.exp) {
            println!("Expiration time is not in the future");
            return Err(AuthenticationError::ExpiredToken);
        }

        // Must be in the past. The time is measured in seconds since the UNIX epoch.
        if !(claims.iat < now) {
            println!("Issued at claim is not in the past");
            return Err(AuthenticationError::InvalidClaims);
        }

        // Must be in the past. The time when the user authenticated.
        if !(claims.auth_time < now) {
            println!("Auth time is not in the past");
            return Err(AuthenticationError::InvalidClaims);
        }

        // Must be your Firebase project ID, the unique identifier for your Firebase project,
        // which can be found in the URL of that project's console.
        if claims.aud != self.audience {
            println!("Audience doesn't match {}", self.audience);
            return Err(AuthenticationError::InvalidClaims);
        }

        // Must be "https://securetoken.google.com/<projectId>", where <projectId> is the same project ID used for aud above.
        if claims.iss != self.issuer {
            println!("Issuer doesn't match {}", self.issuer);
            return Err(AuthenticationError::InvalidClaims);
        }

        // Must be a non-empty string and must be the uid of the user or device.
        if claims.sub == "" {
            println!("Subject is an empty string");
            return Err(AuthenticationError::InvalidClaims);
        }

        return Ok(());
    }

    /// Validates a Google Firebase JWT token. Expects the token to conform to the rules described
    /// in https://firebase.google.com/docs/auth/admin/verify-id-tokens#verify_id_tokens_using_a_third-party_jwt_library.
    pub fn validate(&self, jwt: &str) -> Result<FirebaseClaims, AuthenticationError> {
        // Validate header
        let header = self.validate_header(jwt)?;

        // Validate signature

        // `validate_header` already makes sure that we have the key_id present and valid, but we
        // throw an error here, just to be safe.
        let key_id = header.kid.ok_or(AuthenticationError::InvalidKeyId)?;
        let decoding_key = self
            .keys
            .get(&key_id)
            .ok_or(AuthenticationError::InvalidKeyId)?; // If key doesn't exist, the kid value in the header is invalid

        let token = self.validate_signature(jwt, decoding_key)?;

        // Validate payload
        self.validate_claims(&token.claims)?;

        Ok(token.claims)
    }
}

#[cfg(test)]
mod tests;
