use std::net::SocketAddr;
use std::str::FromStr;

mod api;
mod authenticator;

#[tokio::main]
async fn main() {
    let router = api::setup_router();

    let addr_str = "127.0.0.1:8080";
    let addr = SocketAddr::from_str(addr_str).unwrap();

    println!("Listening on {}", addr_str);
    axum::Server::bind(&addr)
        .serve(router.into_make_service())
        .await
        .unwrap();
}
